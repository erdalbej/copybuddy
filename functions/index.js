const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
exports.deleteFunction = functions.database
  .ref('/count')
  .onDelete((snapshot, context) => {
    admin
      .database()
      .ref(`/buddies/${Object.keys(snapshot.val())}`)
      .remove();
  });
